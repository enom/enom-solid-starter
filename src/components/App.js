/**
 * Main application component.
 *
 * @returns {JSX}
 */
export default function App () {
  return (
    <div class='text-blue-500 m-5 text-center'>{__filename}</div>
  )
}
