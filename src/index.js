import { render } from 'solid-js/web'

import './index.sass'
import App from './components/App'
import Provider from './store'

render(() => (
  <Provider>
    <App />
  </Provider>
), document.getElementById('root'))
